;(function ($, window, undefined) {
    var $allDropdowns = $();
    $.fn.dropdownHover = function (options) {
        if('ontouchstart' in document) return this;
        $allDropdowns = $allDropdowns.add(this.parent());

        return this.each(function () {
            var $this = $(this),
                $parent = $this.parent(),
                defaults = {
                    delay: 500,
                    hoverDelay: 0,
                    instantlyCloseOthers: true
                },
                data = {
                    delay: $(this).data('delay'),
                    hoverDelay: $(this).data('hover-delay'),
                    instantlyCloseOthers: $(this).data('close-others')
                },
                showEvent   = 'show.bs.dropdown',
                hideEvent   = 'hide.bs.dropdown',
                settings = $.extend(true, {}, defaults, options, data),
                timeout, timeoutHover;

            $parent.hover(function (event) {
                if(!$parent.hasClass('open') && !$this.is(event.target)) {
                    return true;
                }

                openDropdown(event);
            }, function () {
                // clear timer for hover event
                window.clearTimeout(timeoutHover)
                timeout = window.setTimeout(function () {
                    $this.attr('aria-expanded', 'false');
                    $parent.removeClass('open');
                    $this.trigger(hideEvent);
                }, settings.delay);
            });
            $this.hover(function (event) {
                if(!$parent.hasClass('open') && !$parent.is(event.target)) {
                    return true;
                }

                openDropdown(event);
            });

            // handle submenus
            $parent.find('.dropdown-submenu').each(function (){
                var $this = $(this);
                var subTimeout;
                $this.hover(function () {
                    window.clearTimeout(subTimeout);
                    $this.children('.dropdown-menu').show();
                    $this.siblings().children('.dropdown-menu').hide();
                }, function () {
                    var $submenu = $this.children('.dropdown-menu');
                    subTimeout = window.setTimeout(function () {
                        $submenu.hide();
                    }, settings.delay);
                });
            });

            function openDropdown(event) {
                if($this.parents(".navbar").find(".navbar-toggle").is(":visible")) {
                    return;
                }
                window.clearTimeout(timeout);
                window.clearTimeout(timeoutHover);
                
                timeoutHover = window.setTimeout(function () {
                    $allDropdowns.find(':focus').blur();

                    if(settings.instantlyCloseOthers === true)
                        $allDropdowns.removeClass('open');
                    window.clearTimeout(timeoutHover);
                    $this.attr('aria-expanded', 'true');
                    $parent.addClass('open');
                    $this.trigger(showEvent);
                }, settings.hoverDelay);
            }
        });
    };

    $(document).ready(function () {
        // Brand Filter
          var filterOnClick = function(e, t) {
            t.length && [].forEach.call(t, function(t) {
              t.onclick = function() {
                var t = arrFilter[e][this.value.replace(/_/g, " ")];
                t && (window.location = t)
              }
            })
          },
          moreFilterOnClick = function(e, t) {
            t.length && $("#filterMoreFilter input[type=checkbox]")
              .on("click", function() {
                var e = $(this)
                  .val();
                $(this)
                  .is(":checked") ? $("ul#filter-by-list li.dropdown")[e].style.display = "block" : $("ul#filter-by-list li.dropdown")[e].style.display = "none"
              })
          };
        for (var el in arrFilter) {
          var $el = $("#filter" + el),
            $clEl = $("#filterSide" + el);
          if ($clEl.length) {
            var $elOpt = document.querySelectorAll('[name="filterSide' + el + '[]"]');
            "MoreFilter" != el ? filterOnClick(el, $elOpt) : moreFilterOnClick(el, $elOpt)
          }
          if ($el.length) {
            "Brand" == el ? $el.multiselect({
              listWidth: 240
            }) : $el.multiselect({
              listWidth: 240,
              addSearchBox: !1
            });
            var $elOpt = document.querySelectorAll('[name="filter' + el + '[]"]');
            "MoreFilter" != el ? filterOnClick(el, $elOpt) : moreFilterOnClick(el, $elOpt)
          }
        }
        $(".brandFilter")
          .on("click", function() {
            var e = this.id;
            alert(e)
          })
        /* Tabs Gender */
        $('.tabs-gender-wrap a').hover(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        // Dropdown do not close on click
        $('li.dropdown.shop-bags button').on('click', function (event) {
            $(this).parent().toggleClass("open");
        });

        $('body').on('click', function (e) {
            if (!$('li.dropdown.shop-bags').is(e.target) && $('li.dropdown.shop-bags').has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
                $('li.dropdown.shop-bags').removeClass('open');
            }
        });
        // Owl Carousel
        $("#fashion-wanita-slider").owlCarousel({
          navigation : true,
          navigationText: [
          "<i class='ion-ios-arrow-back'></i>",
          "<i class='ion-ios-arrow-forward'></i>"
          ],
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem : true,
          autoPlay : 3000
        });
        $("#fashion-pria-slider").owlCarousel({
          navigation : true,
          navigationText: [
          "<i class='ion-ios-arrow-back'></i>",
          "<i class='ion-ios-arrow-forward'></i>"
          ],
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem : true,
          autoPlay : 3500
        });
        $("#eksklusif-slider").owlCarousel({
          navigation : true,
          navigationText: [
          "<i class='ion-ios-arrow-back'></i>",
          "<i class='ion-ios-arrow-forward'></i>"
          ],
          pagination : false,
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem : true,
          autoPlay : 4000
        });
        $("#new-arrival-slider").owlCarousel({
            items : 5,
            autoPlay : 4000,
            lazyLoad : true,
            pagination : true,
            navigation : true,
            navigationText: [
              "<i class='ion-ios-arrow-back'></i>",
              "<i class='ion-ios-arrow-forward'></i>"
              ],
        }); 
        $("#related-pdp-slider").owlCarousel({
            items : 6,
            autoPlay : 4000,
            lazyLoad : true,
            pagination : false,
            navigation : true,
            navigationText: [
              "<i class='ion-ios-arrow-back'></i>",
              "<i class='ion-ios-arrow-forward'></i>"
              ],
        }); 
        document.getElementById("form-add2art").reset();
        $('.btn-removal-product').click( function() {
          removeItem(this);
        });
        
        /* Remove item from cart */
        function removeItem(removeButton)
            {
              /* Remove row from DOM and recalc cart total */
              var productRow = $(removeButton).parent().parent();
              productRow.slideUp(fadeTime, function() {
                productRow.remove();
                recalculateCart();
              });
            }
        // Cloud Zoom
        $("div#wrap").mouseover(function() {
            $("div.mousetrap").css({"height":"550px", "width":"430px"});
            $("div.cloud-zoom-big").css({"height":"550px", "width":"430px"});
        });
        
        // More Slide
        $('.more-slide-btn')
        .click(function(e) {
            $('.more-slide')
                .slideToggle(function() {
                    if ($(this)
                        .is(':visible')) {
                        $('.more-slide-btn')
                            .html('<a href="#">less<\/a>');
                    } else {
                        $('.more-slide-btn')
                            .html('<a href="#">more<\/a>');
                    }
                });
            e.preventDefault();
        });
        // apply dropdownHover to all elements with the data-hover="dropdown" attribute
        $('[data-hover="dropdown"]').dropdownHover();
    });
})(jQuery, window);

// Cloud Zoom

(function ($) {
cloudZoom = {
  attach: function (context, settings) {
    items = $('.cloud-zoom:not(cloud-zoom-processed), .cloud-zoom-gallery:not(cloud-zoom-processed)', context);
    if (items.length) {
      items.addClass('cloud-zoom-processed').CloudZoom();
      items.parent().css('float', 'left');
    }
  }
};
})(jQuery);